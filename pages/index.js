import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Digital Work</title>
        <meta name="description" content="Web tasarım, SEO, SEM, Dijital Pazarlama çözümleri sunmaktayız." />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Digital Work
        </h1>

        <p className={styles.description}>
          <a rel="dofollow" href="https://www.ayhancetin.com.tr">Web tasarım</a>, SEO, SEM, Dijital Pazarlama çözümleri sunmaktayız.
        </p>

        <div className={styles.grid}>
          
            <h2>Burun Estetiği &rarr;</h2>
            <p><strong><a rel="dofollow" href="https://www.kastipmerkezi.com.tr/bizden-haberler/kulak-burun-bogaz/rinoplasti-nedir-nasil-yapilir-fiyatlari-ne-kadar/">Burun estetiği</a></strong> burundaki şekil bozukluklarını düzeltmeye yarayan bir ameliyattır. Güzel bir görünüm kişinin kendisini zihnen ve ruhen iyi hissetmesine ve kendine güveninin artmasına neden olur. Burun yüzün merkezinde bulunduğundan yüz güzelliği için önemli bir organdır ve bu yüzden estetik burun ameliyatı tüm dünyada en çok yapılan estetik ameliyatlardan biridir. <strong><a rel="dofollow" href="https://www.kastipmerkezi.com.tr/bizden-haberler/kulak-burun-bogaz/burun-estetigi-fiyatlari-hangi-kriterlere-gore-belirlenmektedir/">Burun estetiği fiyatları</a></strong> ortalaması ameliyatın türüne göre değişmektedir. Burun estetiği konusunda uzman doktordan yardım almak isteyen kişiler fiyat konusunda da bilgi sahibi olmak istemektedirler. Burun estetiği fiyatları konusunda birçok kriter etkili olmaktadır. Öncelikle ameliyatın zorluk derecesi bu konuda etkin rol oynamaktadır. Bazı durumlarda burun estetiği ameliyatınızın bir kısmını devlet karşılayabilir. Burun estetiği hakkında ayrıntılı bilgiyi web sitemizde bulabilirsiniz. Rinoplasti ameliyatı ile burun küçültülebilir, gerekirse büyütülebilir ve şekil bozukları düzeltilebilir.</p>
          
<h2>Neses Bilgi Teknolojileri &rarr;</h2>
<p><strong><a href="https://neses.com.tr/">Neses Bilgi Teknolojileri</a></strong> olarak İşletmeniz için en uygun BT işletme modelini belirliyor, Uçtan uca tüm ihtiyaçlarınıza profesyonel çözümler sunuyoruz. Neses Bilgi Teknolojileri Bilişim sektöründe 10 yılı aşan deneyimli kadrosu ile, Bilişim Teknolojileri Danışmanlık Hizmetleri, Ağ Altyapı Hizmetleri, Güvenlik Hizmetleri, Sunucu &amp; Veri Depolama Hizmetleri başta olmak üzere Bilişim alanında birçok alan da hizmet veren bir bilişim teknoloji şirketidir.</p>
          
          
<h2>Doypack Ambalaj &rarr;</h2>
<p><strong><a href="https://www.doypack.net/ambalaj/doypack-nedir-doypack-kim-buldu-cesitleri-nelerdir/">Doypack</a></strong>, gıda üreticilerinin tüketicilerine taze ve güvenli bir ürün ulaştırmak amacı ile geliştirilmiştir. Doypack ambalaj, dik durmak gayesi ile tasarlanmış olan vakumlu ve kilitli bir pakettir. Doypack ambalaj stoklu ürünler; şık görünümüyle, asil, zarif duruşlu, az adetli alabileceğiniz ambalajlardır. <a href="https://www.doypack.net/"><strong>Doypack Ambalaj</strong></a> yenilikçi ürün ve hizmetleri ile sektöründe fark yaratan bir marka olma yolunda ilerlemektedir. <a href="https://www.doypack.net/ambalaj/kilitli-ambalaj-nedir-kilitli-ambalaj-cesitleri-nelerdir/"><strong>Kilitli doypack ambalaj</strong></a> sunduğu avantajlarla birlikte şık bir tasarıma da sahip olması ile beraber müşterilerin ilgi odağı olmasında bir kriterdir. <a href="https://www.doypack.net/ambalaj/gida-ambalaji-nedir-gida-ambalaji-cesitleri-nelerdir/"><strong>Gıda ambalajı</strong></a>; ürünün tüketiciye korunaklı bir biçimde ulaştırılması için kullanılan bir gıda üretim parçasıdır. Ambalajlar, içerisindeki ürünü dış etkilerden, içerisine yabancı maddelerin girişinden ve bozulmalardan muhafaza etmektedir.</p>
            
         <p><a href="https://www.ayhancetin.com.tr">Web Tasarım</a>, <a href="https://www.rzg.com.tr/hizmetlerimiz/merkezi-iklimlendirme-sistemleri/merkezi-sogutma-sistemleri/">Merkezi Soğutma Sistemleri</a>, <a href="https://www.metinotocekici.com">Oto Çekici</a>, <a href="https://www.moonlife.com.tr">Mobilya</a></p> 
          
          
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className={styles.logo}>
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer>
    </div>
  )
}
